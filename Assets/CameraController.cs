﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Vector3 targetPosition;
    private float step;
    [SerializeField]
    private float speed = 10;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        targetPosition = player.transform.position;
        step = speed * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, Vector3.MoveTowards(transform.position, targetPosition, step).y, transform.position.z);
    }
}
