﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    public Sprite[] spriteArray;
    private SpriteRenderer spriteRenderer;
    public int count = 0;
    public bool facingLeft = true;
    public bool isMoving;
    public float FPS = 12f;
    private float lastTime;
    private float SPF;
    private int framesCounter = 0;
    private float initialScale;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        SPF = 1f / FPS;
        spriteArray = Resources.LoadAll<Sprite>("Female");
        gameObject.GetComponent<SpriteRenderer>().sprite = spriteArray[count];
        count++;
        initialScale = transform.localScale.x;
        //transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
    }


    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            Animate();
        }
        ManageOrientation();
    }

    void ManageOrientation()
    {
        spriteRenderer.flipX = !facingLeft;
    }

    void Animate()
    {
        if ((Time.time - lastTime) >= SPF)
        {
            framesCounter++;
            spriteRenderer.sprite = spriteArray[count];
            count++;
            count = (count == spriteArray.Length) ? 0 : count;
            lastTime = Time.time;
        }
    }
    public bool Grow(float factor, float maxScale)
    {
        if (transform.localScale.x < maxScale)
        {
            transform.localScale = transform.localScale * factor;
            return true;
        }
        else
        {
            return false; //failed to grow
        }
    }
    public void RestoreScale()
    {
        transform.localScale = new Vector2(initialScale, initialScale);
    }
}
