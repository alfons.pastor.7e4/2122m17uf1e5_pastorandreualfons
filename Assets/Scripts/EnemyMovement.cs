﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    GameObject player;
    public Sprite[] spriteArray;
    private Vector3 targetPosition;
    private float step;
    private float speed = 2;
    private bool playerOnPlatform = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Character");

    }

    // Update is called once per frame
    void Update()
    {
        if (playerOnPlatform)
        {
            targetPosition = player.transform.position;
            step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
        }
    }

    public void PlayerEnteredPlatform()
    {
        playerOnPlatform = true;
    }
}
