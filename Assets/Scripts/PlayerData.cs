﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    Sorcerer,
    Warrior,
    Miner
}

public class PlayerData : MonoBehaviour
{
    public string playerName;
    public string playerSurname;
    public PlayerType playerType;
    public float height = 5;
    public float speed;
    public int lifes;
    public float weight = 60;
    public float speedFactor = 130;
    public float SpeedFactor
    {
        get => speedFactor;
        set
        {
            speedFactor = value;
            speed = speedFactor / weight;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = speedFactor / weight;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
